env: Object

const env = {
    BASE_URL: "",
    PASSWORD: "Test123?",
    WRONG_EMAIL: "test",
    WRONG_PASSWORD: "test",
    CORRECT_EMAIL: "",
    CORRECT_PASSWORD: "",
    QTY: "3",
    OUT_OF_STOCK_QTY: "1000",
    PRODUCT: "",
    COUNTRY: ["Poland", "Andorra", "United States"],
    STATE_POLAND: [
        "wielkopolskie",
        "kujawsko-pomorskie",
        "małopolskie",
        "łódzkie",
        "dolnośląskie",
        "lubelskie",
        "lubuskie",
        "mazowieckie",
        "opolskie",
        "podkarpackie",
        "podlaskie",
        "pomorskie",
        "śląskie",
        "świętokrzyskie",
        "warmińsko-mazurskie",
        "zachodniopomorskie"
    ],

    STATE_USA:  [
        "Alabama",
        "Alaska",
        "Arizona",
        "Arkansas",
        "California",
        "Colorado",
        "Connecticut",
        "Delaware",
        "Florida",
        "Georgia",
        "Hawaii",
        "Idaho",
        "Illinois",
        "Indiana",
        "Iowa",
        "Kansas",
        "Kentucky",
        "Louisiana",
        "Maine",
        "Maryland",
        "Massachusetts",
        "Michigan",
        "Minnesota",
        "Mississippi",
        "Missouri",
        "Montana",
        "Nebraska",
        "Nevada",
        "New Hampshire",
        "New Jersey",
        "New Mexico",
        "New York",
        "North Carolina",
        "North Dakota",
        "Ohio",
        "Oklahoma",
        "Oregon",
        "Pennsylvania",
        "Rhode Island",
        "South Carolina",
        "South Dakota",
        "Tennessee",
        "Texas",
        "Utah",
        "Vermont",
        "Virginia",
        "Washington",
        "West Virginia",
        "Wisconsin",
        "Wyoming"
    ]
}

if (process.env.ENVIROnMENT === "scandi") {
    env.BASE_URL = "https://magento2-demo.scandiweb.com/"
    env.CORRECT_EMAIL = "roni_cost@example.com"
    env.CORRECT_PASSWORD = "roni_cost3@example.com"
    env.PRODUCT = "radiant-tee.html"
}

else if (process.env.ENVIRONMENT === "luma") {
    env.BASE_URL = "https://magento.softwaretestingboard.com/"
}

export default env