import { faker } from "@faker-js/faker"

export class User {
    firstName: string
    lastName: string
    email: string
    company: string
    address_1: string
    address_2: string
    state: string
    city: string
    zipCode: string
    phoneNumber: string

    constructor() {}
}

export function createRandomUser(): User {
    const gender = faker.person.sexType()
    const firstName = faker.person.firstName(gender)
    const lastName = faker.person.lastName(gender)
    const email = firstName + lastName + "@example.pl"
    const company = faker.company.name()
    const address_1 = faker.location.street()
    const address_2 = faker.location.secondaryAddress()
    const state = faker.location.state()
    const city = faker.location.city()
    const zipCode = faker.location.zipCode()
    const phoneNumber = faker.phone.number()


    return {
        firstName,
        lastName,
        email,
        company,
        address_1,
        address_2,
        state,
        city,
        zipCode,
        phoneNumber
    }
}