import { Locator, Page } from "@playwright/test";
import env from "../../env";
import { randomValueGenerator } from "../../helpers/randomValueGenerator";

export class ProductsListingPage {
    readonly page: Page
    readonly productsList: Locator
    readonly addToCartButton: Locator
    readonly addToWishlistLink: Locator
    readonly chooseSizeLabel: Locator
    readonly chooseColorLabel: Locator
    readonly successMessage: Locator
    readonly productItemInfo: Locator

    constructor (page: Page) {
        this.page = page
        this.productsList = page.locator(".item.product.product-item")
        this.addToWishlistLink = page.getByRole("button", {name: "Add to Wish List"})
        this.addToCartButton = page.getByRole("button", { name: 'Add to Cart'})
        this.chooseSizeLabel = page.getByRole("listbox").first().getByRole("option")
        this.chooseColorLabel = page.getByRole("listbox").last().getByRole("option")
        this.successMessage = page.locator('[data-ui-id="message-success"]')
        this.productItemInfo = page.locator('.product-item-info')
    }

    async visit() {
        await this.page.goto(env.BASE_URL + "men/tops-men/jackets-men.html", {waitUntil: "load"})
    }

    async hoverToProductCard(productPosition: number) {
        await this.productsList.nth(productPosition).hover()
    }

    async addProductToWishlist(productPosition: number) {
        await this.addToWishlistLink.nth(productPosition).click()
    }


    async chooseOptions(productPosition: number) {
        let productInfo = await this.productItemInfo.nth(productPosition)
        let sizeCounter = await productInfo.locator(this.chooseSizeLabel).count() 
        await productInfo.locator(this.chooseSizeLabel.nth(randomValueGenerator(sizeCounter))).check()
        let colorCounter = await productInfo.locator(this.chooseColorLabel).count()
        await productInfo.locator(this.chooseColorLabel.nth(randomValueGenerator(colorCounter))).check()
    }

    async addToCart(productPosition: number) {
        await this.addToCartButton.nth(productPosition).click()
    }

}