import { Locator, Page } from "@playwright/test";

export class AccountPage {
    readonly page: Page
    readonly myWishlistLink: Locator

    constructor(page: Page) {
        this.page = page
        this.myWishlistLink = page.getByRole("link", { name: "My Wish List"})
    }
    
    async myWishlistClik() {
        await this.myWishlistLink.click()
    }
}