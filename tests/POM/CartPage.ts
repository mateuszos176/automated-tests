import { Locator, Page, expect } from "@playwright/test";
import env from "../../env";

export class CartPage {
    readonly page: Page
    readonly deleteProduct: Locator
    readonly emptyCartText: Locator
    readonly toCheckoutButton: Locator
    readonly editParametersButton: Locator
    readonly cartTotals: Locator
    

    constructor (page: Page) {
        this.page = page
        this.deleteProduct = page.getByRole("link", {name: "Remove item"})
        this.emptyCartText = page.locator('#maincontent').getByText("You have no items in your shopping cart.")
        this.toCheckoutButton = page.getByRole("button", { name: 'Proceed to Checkout'})
        this.editParametersButton = page.locator(('#maincontent')).getByRole("link", { name: "Edit" })
        this.cartTotals = page.locator("#cart-totals .table-wrapper")
    }

    async visit() {
        await this.page.goto(env.BASE_URL + "checkout/cart/")
    }

    async deleteProductFromCart() {
        await this.deleteProduct.click()
    }

    async clickToProceedToChekcout() {
        await this.toCheckoutButton.click()
    }

}