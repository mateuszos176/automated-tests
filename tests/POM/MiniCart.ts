import { Locator, Page, expect } from "@playwright/test";
import env from "../../env";

export class MiniCart {
    readonly page: Page
    readonly viewCartLink: Locator
    readonly cartIcon: Locator

    constructor(page: Page) {
        this.page = page
        this.viewCartLink = page.locator(".minicart-wrapper .actions .secondary").getByText('View and Edit Cart')
        this.cartIcon = page.locator(".minicart-wrapper .action.showcart")
       
    }

    async viewAndEditCart() {
        await this.cartIcon.click()
        await this.viewCartLink.isVisible()
        await this.viewCartLink.click()
    }
}