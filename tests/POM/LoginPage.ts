import { Locator, Page } from "@playwright/test";
import env from "../../env";

export class LoginPage {
    readonly page: Page
    readonly loginInput: Locator
    readonly passwordInput: Locator
    readonly signInButton: Locator
    readonly signUpButton: Locator
    readonly forgotPasswordLink: Locator
    readonly errorMessage: Locator
   

    constructor(page: Page) {
        this.page = page
        this.loginInput = page.getByLabel("Email", { exact: true })
        this.passwordInput = page.getByLabel("Password", { exact: true })
        this.signInButton = page.getByRole("button", { name: "Sign In"})
        this.signUpButton = page.locator("#maincontent").getByRole("link", {name: "Create an Account"})
        this.forgotPasswordLink = page.getByRole("link", { name: "Forgot Your Password?"})
        this.errorMessage = page.locator('[data-ui-id="message-error"]')
    }

    async visit() {
        await this.page.goto(env.BASE_URL + "customer/account/login/")
    }

    async singIn(email: string, password: string) {
        await this.loginInput.fill(email)
        await this.passwordInput.fill(password)
        await this.signInButton.click()
    }

    async createAccount() {
        await this.signUpButton.click()
    }

    async forgotPassword() {
        await this.forgotPasswordLink.click()
    }    

}