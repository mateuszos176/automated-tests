import { Locator, Page } from "@playwright/test";
import env from "../../env";

export class WishlistPage {
    readonly page: Page
    readonly emptyListMessage: Locator
    readonly successMessage: Locator

    constructor(page: Page) {
        this.page = page
        this.emptyListMessage = page.locator(".message .info .empty")
        this.successMessage = page.locator('[data-ui-id="message-success"]')
    }

    async visit() {
        await this.page.goto(env.BASE_URL + "wishlist/")
    }
    
}