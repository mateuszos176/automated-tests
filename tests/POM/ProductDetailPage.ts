import { Locator, Page } from "@playwright/test";
import env from "../../env";
import { randomValueGenerator } from "../../helpers/randomValueGenerator";

var size

export class ProductDetailPage {
    readonly page: Page
    readonly addToCartButton: Locator
    readonly chooseSizeLabel: Locator
    readonly chooseColorLabel: Locator
    readonly qtyInput: Locator
    readonly addToWishlistLink: Locator
    readonly addToCompareLink: Locator
    readonly successMessage: Locator
    readonly errorMessage: Locator

    constructor (page: Page) {
        this.page = page
        this.addToCartButton = page.getByRole("button", {name: "Add to Cart", exact: true})
        this.addToWishlistLink = page.getByRole("link", {name: "Add to Wish List"})
        this.chooseSizeLabel = page.getByRole("listbox").first().getByRole("option")
        this.chooseColorLabel = page.getByRole("listbox").last().getByRole("option")
        this.qtyInput = page.getByRole('spinbutton', { name: 'Qty' })
        this.successMessage = page.locator('[data-ui-id="message-success"]')
        this.errorMessage = page.locator('[data-ui-id="message-error"]')
    }

    async visit() {
        await this.page.goto(env.BASE_URL + env.PRODUCT)
    }

    async addToWishlist() {
        await this.addToWishlistLink.click()
    }

    async addToCart(quantity: string) {
        let sizeCounter = await this.chooseSizeLabel.count()
        await this.chooseSizeLabel.nth(randomValueGenerator(sizeCounter)).check()
        let colorCounter = await this.chooseColorLabel.count()
        await this.chooseColorLabel.nth(randomValueGenerator(colorCounter)).check()
        await this.qtyInput.fill(quantity)
        await this.addToCartButton.click()
    }
}