import { Locator, Page, expect } from "@playwright/test";
import env from "../../env";
import { randomValueGenerator } from "../../helpers/randomValueGenerator";

export class CheckoutPage {
    readonly page: Page
    readonly email: Locator
    readonly firstName: Locator
    readonly lastName: Locator
    readonly company: Locator
    readonly address: Locator
    readonly country: Locator
    readonly selectState: Locator
    readonly textboxState: Locator
    readonly city: Locator
    readonly zipCode: Locator
    readonly phoneNumber: Locator
    readonly shippingMethod: Locator
    readonly nextButton: Locator
    readonly placeOrder: Locator
    readonly billingAndShippingAddressAreTheSame
    readonly updateBillingAddressButton
    readonly firstNameBilling: Locator
    readonly lastNameBilling: Locator
    readonly companyBilling: Locator
    readonly addressBilling: Locator
    readonly countryBilling: Locator
    readonly selectStateBilling: Locator
    readonly textboxStateBilling: Locator
    readonly cityBilling: Locator
    readonly zipCodeBilling: Locator
    readonly phoneNumberBilling: Locator

    constructor(page: Page) {
        this.page = page
        this.email =  page.getByRole('textbox', { name: 'Email Address*' })
        this.firstName = page.getByLabel("First Name")
        this.lastName = page.getByLabel("Last Name")
        this.company = page.getByLabel("Company")
        this.address = page.getByLabel("Street Address")
        this.country = page.getByLabel("Country")
        this.selectState = page.locator('select[name="region_id"]')
        this.textboxState = page.getByRole('textbox', { name: 'State/Province' })
        this.city = page.getByLabel("City")
        this.zipCode = page.getByLabel("Zip/Postal Code")
        this.phoneNumber = page.getByLabel("Phone Number")
        this.nextButton = page.getByRole("button", { name: "Next" })
        this.placeOrder = page.getByRole("button", { name: "Place Order" })
        this.shippingMethod = page.getByRole("radio")
        this.billingAndShippingAddressAreTheSame = page.locator("#billing-address-same-as-shipping-cashondelivery")
        this.updateBillingAddressButton = page.getByRole("button", { name: "Update" })
        this.firstNameBilling = page.locator(".billing-address-form").getByLabel("First Name")
        this.lastNameBilling = page.locator(".billing-address-form").getByLabel("Last Name")
        this.companyBilling = page.locator(".billing-address-form").getByLabel("Company")
        this.addressBilling = page.locator(".billing-address-form").getByLabel("Street Address")
        this.countryBilling = page.locator(".billing-address-form").getByLabel("Country")
        this.selectStateBilling = page.locator(".billing-address-form").locator('select[name="region_id"]')
        this.textboxStateBilling = page.locator(".billing-address-form").getByRole('textbox', { name: 'State/Province' })
        this.cityBilling = page.locator(".billing-address-form").getByLabel("City")
        this.zipCodeBilling = page.locator(".billing-address-form").getByLabel("Zip/Postal Code")
        this.phoneNumberBilling = page.locator(".billing-address-form").getByLabel("Phone Number")
    }


    async fillAdressData(email: string, name: string, lastName: string, company: string, address_1: string,
         address_2: string, state: string, city: string, zipCode: string, phoneNumber: string) {
        await this.email.fill(email)
        await this.firstName.fill(name)
        await this.lastName.fill(lastName)
        await this.company.fill(company)
        await this.address.first().fill(address_1)
        await this.address.last().fill(address_2)
        let selectedCountry = env.COUNTRY[await randomValueGenerator(env.COUNTRY.length)]
        await this.country.selectOption(selectedCountry)
        if (await this.selectState.isVisible()) {
            await this.selectState.click()
            if (selectedCountry === "Poland")
                await this.selectState.selectOption(env.STATE_POLAND[randomValueGenerator(env.STATE_POLAND.length)])
            else 
                await this.selectState.selectOption(env.STATE_USA[randomValueGenerator(env.STATE_USA.length)])
        } else {
            await this.textboxState.fill(state)
        }
        await this.city.fill(city)
        await this.zipCode.fill(zipCode)
        await this.phoneNumber.fill(phoneNumber)
    }

    async chooseShippingMethod() {
        await this.shippingMethod.last().check()
    }

    async nextClick() {
        await this.nextButton.click()
    }

    async placeOrderClick() {
        await this.placeOrder.click()
    }

    async billingAndShippingAddressAreTheSameUncheking() {
        await this.billingAndShippingAddressAreTheSame.click()
    }

    async updateBillingAddressClick() {
        await this.updateBillingAddressButton.click()
    }

    async fillBillingAddressData(name: string, lastName: string, company: string, address_1: string,
        address_2: string, state: string, city: string, zipCode: string, phoneNumber: string) {
        await this.firstNameBilling.fill(name)
        await this.lastNameBilling.fill(lastName)
        await this.companyBilling.fill(company)
        await this.addressBilling.first().fill(address_1)
        await this.addressBilling.last().fill(address_2)
        let selectedCountry = env.COUNTRY[await randomValueGenerator(env.COUNTRY.length)]
        await this.countryBilling.selectOption(selectedCountry)
        if (await this.selectStateBilling.isVisible()) {
            await this.selectStateBilling.click()
            if (selectedCountry === "Poland")
                await this.selectStateBilling.selectOption(env.STATE_POLAND[randomValueGenerator(env.STATE_POLAND.length)])
            else 
                await this.selectStateBilling.selectOption(env.STATE_USA[randomValueGenerator(env.STATE_USA.length)])
        } else {
            await this.textboxStateBilling.fill(state)
        }
        await this.cityBilling.fill(city)
        await this.zipCodeBilling.fill(zipCode)
        await this.phoneNumberBilling.fill(phoneNumber)
    }
}