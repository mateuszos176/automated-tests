import { Locator, Page } from "@playwright/test"
import env from "../../env"

export class RegistrationPage {
    readonly page: Page
    readonly firstNameInput: Locator
    readonly lastNameInput: Locator
    readonly emailInput: Locator
    readonly passwordInput: Locator
    readonly confirmPasswordInput: Locator
    readonly signUpButton: Locator
    readonly newsletterCheckbox: Locator
    readonly errorMessage: Locator
    readonly alreadyExistedAccountAlert: Locator
    

    constructor(page: Page) {
        this.page = page
        this.firstNameInput = page.getByLabel("First Name")
        this.lastNameInput = page.getByLabel("Last Name")
        this.emailInput = page.getByLabel("Email", { exact: true })
        this.passwordInput = page.getByLabel("Password", { exact: true }).first()
        this.confirmPasswordInput = page.getByLabel("Confirm Password", { exact: true })
        this.signUpButton = page.getByRole("button", {name: "Create an Account"})
        this.newsletterCheckbox = page.getByRole("checkbox", {name: "Sign Up for Newsletter"})
        this.errorMessage = page.locator(".mage-error").first()
        this.alreadyExistedAccountAlert = page.locator('[data-ui-id="message-error"]')
    }

    async visit() {
        await this.page.goto(env.BASE_URL + "customer/account/create/")
    }

    async signUp(firstName: string, lastName: string, email: string, password: string) {
        await this.firstNameInput.fill(firstName)
        await this.lastNameInput.fill(lastName)
        await this.emailInput.fill(email)
        await this.passwordInput.fill(password)
        await this.confirmPasswordInput.fill(password)
        await this.signUpButton.click()
    }

    async checkNewslleter() {
        await this.newsletterCheckbox.check()
    }

}

