import { test, expect } from '@playwright/test'
import { RegistrationPage } from "./POM/RegistrationPage"
import { User, createRandomUser } from '../helpers/User'
import env from '../env'

test.describe("Registration page", () => {
    let registrationPage: RegistrationPage
    let user: User
    test.beforeEach(async ({page, context}) => {
        registrationPage = new RegistrationPage(page)
        user = createRandomUser()
    })

    test("Registration without checking optional checkboxes", async ({page}) => {
        await registrationPage.visit()
        await registrationPage.signUp(user.firstName, user.lastName, user.email, env.PASSWORD)
        await expect(page).toHaveURL(env.BASE_URL + "customer/account/")
    })

    test("Registration with check newsletter and checking newsletter is subscribed", async ({page}) => {
        await registrationPage.visit()
        await registrationPage.checkNewslleter()
        await registrationPage.signUp(user.firstName, user.lastName, user.email, env.PASSWORD)
        await expect(page).toHaveURL(env.BASE_URL + "customer/account/")
        await expect(page.getByText('You are subscribed to "General Subscription".')).toBeVisible()
        
    })

    test("Registration failed - wrong email format", async() => {
        await registrationPage.visit()
        await registrationPage.signUp(user.firstName, user.lastName, env.WRONG_EMAIL , env.PASSWORD)
        await expect(registrationPage.errorMessage).toBeVisible()
    })

    test("Registration failed - wrong password format", async() => {
        await registrationPage.visit()
        await registrationPage.signUp(user.firstName, user.lastName, user.email , env.WRONG_EMAIL)
        await expect(registrationPage.errorMessage).toBeVisible()
    })

    test("account with this email address is already exist", async({page}) => {
        await registrationPage.visit()
        await registrationPage.signUp(user.firstName, user.lastName, env.CORRECT_EMAIL , env.PASSWORD)
        await expect(registrationPage.alreadyExistedAccountAlert).toBeEnabled()
    })
})