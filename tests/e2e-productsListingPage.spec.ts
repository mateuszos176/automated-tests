import { test, expect} from '@playwright/test';
import { LoginPage } from './POM/LoginPage';
import { AccountPage } from './POM/AccountPage';
import { WishlistPage } from './POM/WishlistPage';
import { ProductsListingPage } from './POM/ProductsListingPage';
import { randomValueGenerator } from '../helpers/randomValueGenerator';
import env from '../env';


test.describe("PLP tests", () => {
    let loginPage: LoginPage
    let accountPage: AccountPage
    let wishlistPage: WishlistPage
    let productsListingPage: ProductsListingPage

    test.beforeEach(async ({ page, context }) => {
        productsListingPage = new ProductsListingPage(page)
        loginPage = new LoginPage(page)
        wishlistPage = new WishlistPage(page)
    })

    test("Not logged user want to add product (from PLP) to wishlist", async () => {
        await productsListingPage.visit()
        let numberOfProducts = await productsListingPage.productsList.count()
        let productPosition = await randomValueGenerator(numberOfProducts)
        await productsListingPage.hoverToProductCard(productPosition)
        await productsListingPage.addProductToWishlist(productPosition)
        await loginPage.errorMessage.waitFor()
    })


    test("Logged user add product to whislit (from PLP)", async () => {
        await loginPage.visit()
        await loginPage.singIn(env.CORRECT_EMAIL, env.CORRECT_PASSWORD)
        await productsListingPage.visit()
        let numberOfProducts = await productsListingPage.productsList.count()
        let productPosition = await randomValueGenerator(numberOfProducts)
        await productsListingPage.hoverToProductCard(productPosition)
        await productsListingPage.addProductToWishlist(productPosition)
        // To refactoring. Maybe create component from message
        await wishlistPage.successMessage.waitFor()
    })

    test("User add product to cart", async () => {
        await productsListingPage.visit()
        let numberOfProducts = await productsListingPage.productsList.count()
        let productPosition = await randomValueGenerator(numberOfProducts)
        await productsListingPage.hoverToProductCard(productPosition)
        await productsListingPage.chooseOptions(productPosition)
        await productsListingPage.addToCart(productPosition)
        await productsListingPage.successMessage.waitFor()
    })

    test("User add product to cart without checked size and color", async ({ page }) => {
        await productsListingPage.visit()
        let numberOfProducts = await productsListingPage.productsList.count()
        let productPosition = await randomValueGenerator(numberOfProducts)
        await productsListingPage.hoverToProductCard(productPosition)
        await productsListingPage.addToCart(productPosition)
        await page.waitForSelector('[data-ui-id="message-notice"]')
    })

})