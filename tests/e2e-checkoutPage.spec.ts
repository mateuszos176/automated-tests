import { test, expect, Browser, firefox} from '@playwright/test';
import { CartPage } from './POM/CartPage';
import { ProductDetailPage } from './POM/ProductDetailPage';
import env from '../env';
import { MiniCart } from './POM/MiniCart';
import { User, createRandomUser } from '../helpers/User'
import { CheckoutPage } from './POM/CheckoutPage';
import { LoginPage } from './POM/LoginPage'
import { allure } from "allure-playwright"


test.describe("Checkout test", () => {
    let cartPage: CartPage
    let productDetailPage: ProductDetailPage
    let miniCart: MiniCart
    let checkoutPage: CheckoutPage
    let loginPage: LoginPage
    let user: User

    test.beforeEach(async ({ page, context}) => {
        allure.epic("Checkout")
        cartPage = new CartPage(page)
        productDetailPage = new ProductDetailPage(page)
        miniCart = new MiniCart(page)
        checkoutPage = new CheckoutPage(page)
        loginPage = new LoginPage(page)
        user = createRandomUser()
    })

    test("user removing products from Cart", async () => {
        await productDetailPage.visit()
        await productDetailPage.addToCart(env.QTY)
        await cartPage.visit()
        
        while (await cartPage.emptyCartText.isDisabled()) {
            await cartPage.deleteProductFromCart() 
        }

        await expect(cartPage.emptyCartText).toBeEnabled()
    })

    test("Not logged user go to checkout and want to buy products", async ({ page }) => {
        await productDetailPage.visit()
        await productDetailPage.addToCart(env.QTY)
        await expect(productDetailPage.successMessage).toBeEnabled()
        await miniCart.viewAndEditCart()
        await expect(page).toHaveURL(env.BASE_URL + "checkout/cart/")
        await cartPage.cartTotals.waitFor()
        await cartPage.clickToProceedToChekcout()
        await expect(page).toHaveURL(env.BASE_URL + "checkout/#shipping")
        await checkoutPage.fillAdressData(user.email, user.firstName, user.lastName, user.company, user.address_1,
             user.address_2, user.state, user.city, user.zipCode, user.phoneNumber)

        await page.waitForTimeout(2000)
        await checkoutPage.chooseShippingMethod()
        await checkoutPage.nextClick()
        //await page.waitForLoadState('networkidle')
        await page.waitForURL(env.BASE_URL + 'checkout/#payment')
        await checkoutPage.placeOrderClick()
        await expect(page).toHaveURL(env.BASE_URL + "checkout/onepage/success/")

    })

    test("Not logged user want to buy products but billing address != shipping", async ({page}) => {
        await productDetailPage.visit()
        await productDetailPage.addToCart(env.QTY)
        await expect(productDetailPage.successMessage).toBeEnabled()
        await miniCart.viewAndEditCart()
        await expect(page).toHaveURL(env.BASE_URL + "checkout/cart/")
        await cartPage.cartTotals.waitFor()
        await cartPage.clickToProceedToChekcout()
        await expect(page).toHaveURL(env.BASE_URL + "checkout/#shipping")
        await checkoutPage.fillAdressData(user.email, user.firstName, user.lastName, user.company, user.address_1,
             user.address_2, user.state, user.city, user.zipCode, user.phoneNumber)

        await page.waitForTimeout(2000)
        await checkoutPage.chooseShippingMethod()
        await checkoutPage.nextClick()
        await page.waitForURL(env.BASE_URL + 'checkout/#payment')
        await checkoutPage.billingAndShippingAddressAreTheSameUncheking()
        await checkoutPage.fillBillingAddressData(user.firstName, user.lastName, user.company, user.address_1,
            user.address_2, user.state, user.city, user.zipCode, user.phoneNumber)
        
        await checkoutPage.updateBillingAddressClick()
        await checkoutPage.placeOrderClick()
        await expect(page).toHaveURL(env.BASE_URL + "checkout/onepage/success/")

    })

    test("Logged user want checkout test", async ({ page }) => {
        await loginPage.visit()
        await loginPage.singIn(env.CORRECT_EMAIL, env.CORRECT_PASSWORD)
        await productDetailPage.visit()
        await productDetailPage.addToCart(env.QTY)
        await expect(productDetailPage.successMessage).toBeEnabled()
        await miniCart.viewAndEditCart()
        await expect(page).toHaveURL(env.BASE_URL + "checkout/cart/")
        await cartPage.cartTotals.waitFor()
        await cartPage.clickToProceedToChekcout()
        await checkoutPage.chooseShippingMethod()
        await checkoutPage.nextClick()
        await page.waitForURL(env.BASE_URL + 'checkout/#payment')
        await checkoutPage.placeOrderClick()
        await expect(page).toHaveURL(env.BASE_URL + "checkout/onepage/success/")

    })

})