import { test, expect} from '@playwright/test'
import env from '../env'
import { LoginPage } from './POM/LoginPage'

test.describe("Login page", () => {
    let loginPage: LoginPage
    

    test.beforeEach(async ({ page, context}) => {
        //allure.epic("Login Page")
        loginPage = new LoginPage(page)
        
    })

    test("login to account", async ({page}) => {
        await loginPage.visit()
        await loginPage.singIn(env.CORRECT_EMAIL, env.CORRECT_PASSWORD)
        await expect(page).toHaveURL(env.BASE_URL + "customer/account/")
    })

    test("Go to Create Account", async ({page}) => {
        await loginPage.visit()
        await loginPage.createAccount()
        await expect(page).toHaveURL(env.BASE_URL + "customer/account/create/")
    })

    test("Click Forgot password link", async ({page}) => {
        await loginPage.visit()
        await loginPage.forgotPassword()
        await expect(page).toHaveURL(env.BASE_URL + "customer/account/forgotpassword/")
    })

    test("input incorrect data", async () => {
        await loginPage.visit()
        await loginPage.singIn(env.CORRECT_EMAIL, env.PASSWORD)
        await expect(loginPage.errorMessage ).toBeEnabled()
    })
})