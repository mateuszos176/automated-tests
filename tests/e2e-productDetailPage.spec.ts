import { test, expect } from '@playwright/test'
import { ProductDetailPage } from './POM/ProductDetailPage'
import { LoginPage } from './POM/LoginPage'
import env from '../env'


test.describe("PDP", () => {
    let productDetailPage: ProductDetailPage
    let loginPage: LoginPage

    test.beforeEach(async ({page, context}) => {
        productDetailPage = new ProductDetailPage(page)
        loginPage = new LoginPage(page)
    })

    test("Logged in user add product to wishlist", async ({page}) => {
        await loginPage.visit()
        await loginPage.singIn(env.CORRECT_EMAIL, env.CORRECT_PASSWORD)
        await expect(page).toHaveURL(env.BASE_URL + "customer/account/")
        await productDetailPage.visit()
        await productDetailPage.addToWishlist()
        // To do when i create wihslistPage (checking succes popup is dipslay and added product is wishlist)
    })

    test("Not logged user want to add product to whishlist", async ({ page }) => {
        await productDetailPage.visit()
        await productDetailPage.addToWishlist()
        await expect(page).toHaveURL(env.BASE_URL + "customer/account/login/")
        await expect(loginPage.errorMessage).toBeEnabled()
    })

    test("Logged User add product to cart", async ({page}) => {
        await loginPage.visit()
        await loginPage.singIn(env.CORRECT_EMAIL, env.CORRECT_PASSWORD)
        await productDetailPage.visit()
        await productDetailPage.addToCart(env.QTY)
        await productDetailPage.successMessage.waitFor()
        await expect(productDetailPage.successMessage).toBeEnabled()
    })

    test("User want to add product with quantity > in stock", async () => {
        await loginPage.visit()
        await loginPage.singIn(env.CORRECT_EMAIL, env.CORRECT_PASSWORD)
        await productDetailPage.visit()
        await productDetailPage.addToCart(env.OUT_OF_STOCK_QTY)
        await productDetailPage.errorMessage.waitFor()
        await expect(productDetailPage.errorMessage).toBeEnabled()
    })

    test("Not Logged user add product to cart", async () => {
        await productDetailPage.visit()
        await productDetailPage.addToCart(env.QTY)
        await productDetailPage.successMessage.waitFor()
        await expect(productDetailPage.successMessage).toBeEnabled()
    })
})